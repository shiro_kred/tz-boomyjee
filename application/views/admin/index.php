<?php defined('SYSPATH') or die('No direct script access.'); ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?= isset($description) ? $description : '' ?>">

    <title><?= isset($title) ? $title : '' ?></title>

    <link href="/media/libs/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/media/base/css/style.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="main">
<div class="col-lg-12">
    <div class="col-lg-10 col-lg-offset-1 reply-list">
        <h1>Список отзывов</h1>

        <table class="table table-hover col-lg-12">
            <thead>
            <tr>
                <td class="element-reply-sort col-lg-1" data-sort="photo">Id</td>
                <td class="element-reply-sort col-lg-1" data-sort="photo">Photo</td>
                <td class="element-reply-sort col-lg-2" data-sort="email">Email</td>
                <td class="element-reply-sort col-lg-6" data-sort="text">Text</td>
                <td class="element-reply-sort col-lg-1" data-sort="create">Date</td>
                <td class="element-reply-sort col-lg-1" data-sort="status">Create</td>
            </tr>
            </thead>
            <tbody id="loader-reply" style="font-size: 10px;">
            <?php foreach ($data as $item):?>
                <tr>
                    <td><?=$item['id']?></td>
                    <td><?= isset($item['photo']) ? "<img src='".$item['photo']."'>" : '';?></td>
                    <td><?=$item['email']?></td>
                    <td><?=$item['text']?></td>
                    <td><?=$item['create']?></td>
                    <td><?= ($item['status']) ? ($item['status']==2) ? 'Отредактирован' :'Опубликован' : 'Не опубликован';?></td>
                    <td><a href="/admin/delete/<?=$item['id']?>">Удалить</a></td>
                    <td><a href="/admin/edit/<?=$item['id']?>">Изменить</a></td>
                    <td><a href="/admin/active/<?=$item['id']?>">Опубликовать</a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/media/libs/bootstrap/js/bootstrap.js"></script>
<script src="/media/libs/bootstrap/js/validator.js"></script>
<script src="/media/base/js/libs/underscore/underscore-min.js" type="text/javascript"></script>
<script src="/media/base/js/libs/backbone/backbone.min.js" type="text/javascript"></script>
<script src="/media/base/app/app.js" type="text/javascript"></script>
</body>
</html>
