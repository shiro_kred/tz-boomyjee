<?php defined('SYSPATH') or die('No direct access allowed.');

class api extends base_api
{
    /**
     * Check Status for API
     */
    public function status()
    {
        $this->status = false;
        $this->message = 'ShiroApi';
        $this->data = [
            'version' => 1,
            'method' => 'checkStatus'
        ];
        $this->query = null;
        parent::response();
    }

    public function getList()
    {
        $query = "SELECT * FROM `reply` WHERE `reply`.`status` = 1";
        if (isset($_GET['sort']) and (($_GET['sort'] == 'ASC') or ($_GET['sort'] == 'DESC'))) {
            $sort = $_GET['sort'];
        } else {
            $sort = 'ASC';
        }

        if (isset($_GET['order_by'])) {
            $query = $query . "ORDER BY `reply`.`" . htmlspecialchars($_GET['order_by']) . "` " . $sort;
        }
        $this->data = Database::query($query);
        $this->status = true;
        parent::response();
    }

    public function reply()
    {
        if (isset($_POST['photo'])) {
            $name = Text::random('alnum', 32) . '.jpg';
            $upload = DOCROOT . 'media' . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR;
            $this->base64_to_jpeg($_POST['photo'], $upload . $name);
            $this->resizeImage($upload . $name, 640, 640, 1);
        }

        $date = date("Y-m-d", time());
        $query = "INSERT INTO `reply` (`email`, `text`, `photo`, `create`) VALUES ('" . $_POST['email'] . "', '" . $_POST['text'] . "', '" . $name . "', '".$date."')";
        $this->data = Database::query($query);
        $this->status = true;
        parent::response();
    }

    private function base64_to_jpeg($base64_string, $output_file)
    {
        $ifp = fopen($output_file, "wb");

        $data = explode(',', $base64_string);

        fwrite($ifp, base64_decode($data[1]));
        fclose($ifp);

        return $output_file;
    }

    private function resizeImage($image,$width,$height,$scale) {
        //generate new image height and width of source image
        $newImageWidth = ceil($width * $scale);
        $newImageHeight = ceil($height * $scale);
        //Create a new true color image
        $newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
        //Create a new image from file
        $source = imagecreatefromjpeg($image);
        //Copy and resize part of an image with resampling
        imagecopyresampled($newImage,$source,0,0,0,0,$newImageWidth,$newImageHeight,$width,$height);
        //Output image to file
        imagejpeg($newImage,$image,90);
        //set rights on image file
        chmod($image, 0777);
        //return crop image
        return $image;
    }
}