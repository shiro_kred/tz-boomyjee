<?php defined('SYSPATH') or die('No direct access allowed.');

class index extends base_action
{
    public function index() {
        $this->View('views/index/index', [
            'title' => 'Home Page',
        ]);
    }
}