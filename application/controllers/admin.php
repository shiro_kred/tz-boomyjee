<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Http Auth
 * login: TestBoobyJee
 * password: TestBoobyJee
 */
if (!isset($_SERVER['PHP_AUTH_USER'])) {
    header("WWW-Authenticate: Basic realm=\"Private Area\"");
    header("HTTP/1.0 401 Unauthorized");
    die("Sorry - you need valid credentials to be granted access!\n");
} else {
    if (($_SERVER['PHP_AUTH_USER'] == 'TestBoobyJee') && ($_SERVER['PHP_AUTH_PW'] == 'TestBoobyJee')) {

    } else {
        header("WWW-Authenticate: Basic realm=\"Private Area\"");
        header("HTTP/1.0 401 Unauthorized");
        die("Sorry - you need valid credentials to be granted access!\n");
    }
}

class admin extends base_action
{
    public $model = null;

    public function index()
    {
        $this->View('views/admin/index', [
            'title' => 'Admin Page',
            'data'  =>  Model_Reply::getList()
        ]);
    }

    public function active()
    {
        if(isset($GLOBALS['param'])) {
            $this->model = Model_Reply::setActive($GLOBALS['param']);
        }

        $this->View('views/admin/index', [
            'title' => 'Admin Page',
            'data'  =>  Model_Reply::getList()
        ]);
    }

    public function delete()
    {
        if(isset($GLOBALS['param'])) {
            $this->model = Model_Reply::deleteItemById($GLOBALS['param']);
        }

        $this->View('views/admin/index', [
            'title' => 'Admin Page',
            'data'  =>  Model_Reply::getList()
        ]);
    }

    public function edit() {
        if(isset($_POST)) {
            Model_Reply::updateItem($GLOBALS['param'], $_POST['email'], $_POST['text']);
            $this->View('views/admin/index', [
                'title' => 'Edit Page',
                'data'  =>  Model_Reply::getList()
            ]);
        } else {
            if(isset($GLOBALS['param'])) {
                $this->model = Model_Reply::getItemById($GLOBALS['param']);
            }

            $this->View('views/admin/edit', [
                'title' => 'Edit Page',
                'model'  => $this->model[0],
                'data'  =>  Model_Reply::getList()
            ]);
        }
    }
}