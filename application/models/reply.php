<?php

Class Model_Reply{
	
    public static function getList() {
        $query = "SELECT * FROM `reply`";
        return Database::query($query);
    }
    
    public static function getItemById($id) {
        $query = "SELECT * FROM `reply` WHERE `reply`.`id` = ".$id;
        return Database::query($query);
    }

    public static function setActive($id) {
        $query = "UPDATE `reply` SET `status` = '1' WHERE `reply`.`id` = ".$id;
        return Database::query($query);
    }

    public static function deleteItemById($id) {
        $query = "DELETE FROM `reply` WHERE `reply`.`id` = ".$id;
        return Database::query($query);
    }

    public static function updateItem($id, $email = null, $text = null) {
        $query = "UPDATE `reply` SET `status` = '2', `email` = '".$email."', `text` = '".$text."' WHERE `reply`.`id` = ".$id;
        return Database::query($query);
    }
}