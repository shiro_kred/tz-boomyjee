<?php defined('SYSPATH') or die('No direct access allowed.');

class Database
{

    public static function query($sql_query, $is_array = true)
    {
        $config = include(SYSPATH . 'config' . DIRECTORY_SEPARATOR . 'database' . EXT);

        if (isset($config['default'])) {
            $config = $config['default'];
        }

        if ($config['type'] == 'MySQL') {
            $config = $config['connection'];
            $db = mysql_connect($config['hostname'], $config['username'], $config['password']) or die('Could not connect: ' . mysql_error());
            mysql_select_db($config['database']) or die('DB not found...');
            $result = mysql_query($sql_query) or die('Query failed: ' . mysql_error());


            if($is_array) {
                $respond = [];
                while ($row = mysql_fetch_object($result)) {
                    $item = [];
                    foreach ($row as $key => $value) {
                        $item[$key] = $value;
                    }
                    $respond[] = $item;
                }

                $result = $respond;
            }

            mysql_close($db);
            return $result;
        }
        return false;
    }
}