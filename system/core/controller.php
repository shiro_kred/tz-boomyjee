<?php defined('SYSPATH') or die('No direct access allowed.');

class Controller
{

    public static function init(array $settings = NULL)
    {
        Route::init();
        Database::init();
    }
}